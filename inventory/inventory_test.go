package inventory

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
)

// TODO: add more tests to cover the error cases

const inventoryFileName = "inventory.json"

type mockStore struct {
	storeArticleFunc func(id uint64, name string, stock uint64) error
}

func (ms mockStore) StoreArticle(id uint64, name string, stock uint64) error {
	return ms.storeArticleFunc(id, name, stock)
}

func createDirIfNotExist(t *testing.T, dir string) {
	t.Helper()
	_, err := os.Stat(dir)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(dir, os.ModePerm)
		assert.NoError(t, errDir)
	}
}

func generateMockFile(t *testing.T, record inventoryFormat) (path string) {
	t.Helper()

	dir := os.TempDir()
	inventoryDir := dir + "inventory-tests/"
	createDirIfNotExist(t, inventoryDir)
	fileName := inventoryDir + inventoryFileName
	b, err := json.Marshal(record)
	_ = ioutil.WriteFile(fileName, b, 0644)

	assert.NoError(t, err)

	return fileName

}

func TestInventoryInsert(t *testing.T) {
	record := inventoryFormat{
		Articles: []article{
			{
				ArtID: "1",
				Name:  "leg",
				Stock: "12",
			},
			{
				ArtID: "2",
				Name:  "screw",
				Stock: "17",
			},
			{
				ArtID: "3",
				Name:  "seat",
				Stock: "2",
			},
			{
				ArtID: "4",
				Name:  "table top",
				Stock: "1",
			},
		},
	}
	path := generateMockFile(t, record)
	var ids []uint64
	var names []string
	var stocks []uint64
	storeFunc := func(id uint64, name string, stock uint64) error {
		ids = append(ids, id)
		names = append(names, name)
		stocks = append(stocks, stock)
		return nil
	}
	store := mockStore{
		storeArticleFunc: storeFunc,
	}
	inventory := NewInventory(store)
	err := inventory.Insert(path)

	assert.NoError(t, err)
	assert.Equal(t, []uint64{1, 2, 3, 4}, ids)
	assert.Equal(t, []string{"leg", "screw", "seat", "table top"}, names)
	assert.Equal(t, []uint64{12, 17, 2, 1}, stocks)

}
