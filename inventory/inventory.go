package inventory

import (
	"encoding/json"
	"fmt"
	"go.uber.org/zap"
	"io/ioutil"
	"os"
	"strconv"
)

type store interface {
	StoreArticle(id uint64, name string, stock uint64) error
}

// Inventory type is used to get store data and insert article records to it
type Inventory struct {
	Store store
}

// NewInventory creates a new Inventory instance assign it's variables and return it
func NewInventory(s store) *Inventory {
	return &Inventory{Store: s}
}

type inventoryFormat struct {
	Articles []article `json:"inventory"`
}
type article struct {
	ArtID string `json:"art_id"`
	Name  string `json:"name"`
	Stock string `json:"stock"`
}

// Insert reads a json file, parse it using inventoryFormat and insert its articles on the store
func (i *Inventory) Insert(path string) error {
	var successful, failed uint64
	// open the json file
	f, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("cannot open the inventory file %w", err)
	}

	// TODO: read the file using a buffer and tokens to support a big json file
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return fmt.Errorf("cannot read the inventory file %w", err)
	}

	var input inventoryFormat
	if err := json.Unmarshal(data, &input); err != nil {
		return fmt.Errorf("cannot unmarshal the inventory file %w", err)
	}

	for _, article := range input.Articles {
		id, err1 := strconv.ParseUint(article.ArtID, 10, 64)
		stock, err2 := strconv.ParseUint(article.Stock, 10, 64)
		if err1 != nil || err2 != nil {
			zap.L().Warn("converting values failed", zap.Errors("errors", []error{err1, err2}))
			failed++
			continue
		}
		if err := i.Store.StoreArticle(id, article.Name, stock); err != nil {
			return fmt.Errorf("storing article failed %w", err)
		}
		successful++
	}
	zap.L().Info("inventory inserted", zap.Uint64("successful", successful), zap.Uint64("failed", failed))
	return nil
}
