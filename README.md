# Warehouse
Warehouse is a tool for inserting inventory and products data from JSON files to permanent storage. The warehouse also provides an API web service for getting product list and purchase them.
## Insert tool
It is a simple tool to process inventory and products data from JSON files and insert them into the store(DB).
### Usage
Set these environment variables for the insert tool to find the inventory and products files and get the DB environment before running the tool
```bash
export INSERT_INVENTORY_PATH="./assignment/inventory.json"
export INSERT_PRODUCTS_PATH="./assignment/products.json"
export INSERT_DB_USER="unicorn_user"
export INSERT_DB_PASSWORD="magical_password"
export INSERT_DB_NAME="warehouse"
```
Use the make command to run a local PostgreSQL container, create a database inside it and do the migration to create required tables
```bash
make postgres
make createdb
make migrateup
```
then you can run the insert tool to parse inventory and products data and add them to the store(DB)
```bash
make insert
```
```json
{"level":"info","ts":1610275172.972877,"msg":"inventory inserted","successful":4,"failed":0}
{"level":"info","ts":1610275173.018812,"msg":"product inserted","successful":6,"failed":0}
```
## API Service
It's a go HTTP service for getting inventory and products data from the store and purchase them

### Usage
Set these environment variables for the API server to get the DB address and server address `host:port` before running the service.
```bash
export INSERT_DB_USER="unicorn_user"
export INSERT_DB_PASSWORD="magical_password"
export INSERT_DB_NAME="warehouse"
export API_SERVER_ADDRESS="localhost:8080"

make server
```
```json
{"level":"info","ts":1610275332.505394,"msg":"starting the server","address":"localhost:8080"}
```

## API endpoint
### Get list and availability of all products
Method: ```GET```
Endpoint: ```/products```
```bash
curl "http://localhost:8080/products"
```
```json
[
  {
    "id": 2,
    "name": "Dinning Table",
    "availability": 1
  },
  {
    "id": 1,
    "name": "Dining Chair",
    "availability": 2
  }
]
```
### Sell Product
Method: ```POST```
Endpoint: ```/products/sell```
```bash
curl -X POST "http://localhost:8080/products/sell" -d "name=Dining Chair"
```
```json
{
  "message": "successful product sell",
  "status": 200
}
```
## Running tests
To run tests pass the short option to skip database-related integration tests
```bash
go test -short $(go list ./... | grep -v cmd) 
```
or you can use the make to run the tests
```bash
make test
```
To run database-related integration tests, you need to set environment variables for the database. Tests will create a new database on the PostgreSQL server `test_db` and insert the test data to run the test. after running the tests, it cleans up and removes the test database from the server
```bash
export INSERT_DB_USER="unicorn_user"
export INSERT_DB_PASSWORD="magical_password"

make test-all
```
