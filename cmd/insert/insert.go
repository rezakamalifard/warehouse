package main

import (
	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/rezakamalifard/warehouse/inventory"
	"gitlab.com/rezakamalifard/warehouse/product"
	"gitlab.com/rezakamalifard/warehouse/storage"
	"go.uber.org/zap"
)

func main() {
	// create the logger
	logger, _ := zap.NewProduction()
	defer func() { _ = logger.Sync() }() // flushes logger buffer before exit if exists
	// set global logger Zap.L() to create logger
	zap.ReplaceGlobals(logger)

	// load configs from environment variables
	var config Config
	if err := cleanenv.ReadEnv(&config); err != nil {
		zap.L().Panic("failed to load config from env", zap.Errors("errors", []error{err}))
	}

	// create the store
	store, err := storage.NewStorage()
	if err != nil {
		zap.L().Panic("failed to create store", zap.Errors("errors", []error{err}))
	}

	// create inventory
	inv := inventory.NewInventory(store)
	if err := inv.Insert(config.InventoryPath); err != nil {
		zap.L().Panic("failed to create new inventory", zap.Errors("errors", []error{err}))
	}
	prod := product.NewProduct(store)
	if err := prod.Insert(config.ProductsPath); err != nil {
		zap.L().Panic("failed to create new product", zap.Errors("errors", []error{err}))
	}
}
