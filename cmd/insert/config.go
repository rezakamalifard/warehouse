package main

// Config is used to get configs from environment variables
type Config struct {
	InventoryPath string `env:"INSERT_INVENTORY_PATH"`
	ProductsPath  string `env:"INSERT_PRODUCTS_PATH"`
}
