APP = warehouse
TIMESTAMP?=$(shell TZ="Asia/Tehran" date +'%Y-%m-%dT%H:%M:%S%z')
GIT_HEAD_REF := $(shell cat .git/HEAD | cut -d' ' -f2)
CI_COMMIT_REF_SLUG?=$(shell cat .git/HEAD | cut -d'/' -f3)
CI_COMMIT_SHORT_SHA?=$(shell cat .git/$(GIT_HEAD_REF) | head -c 8)
DC_FILE="docker-compose.yml"
build-api:
	go build -race  -o bin/server gitlab.com/rezakamalifard/warehouse/cmd/server

build-insert:
	go build -race -o bin/insert gitlab.com/rezakamalifard/warehouse/cmd/insert

build: build-api build-insert

run-insert:
	go run gitlab.com/rezakamalifard/warehouse/cmd/index

run-api:
	go run gitlab.com/rezakamalifard/warehouse/cmd/server

insert: build-insert
	bin/insert

server: build-api
	bin/server

test:
	go test -short `go list ./... | grep -v cmd`

test-all:
	go test `go list ./... | grep -v cmd`

coverage:
	go test -race -short -v -coverprofile=.testCoverage.txt ./...
	go tool cover -func=.testCoverage.txt

coverage-report: coverage
	go tool cover -html=.testCoverage.txt -o testCoverageReport.html

postgres:
	docker run --name postgres13 -p 5432:5432 -e POSTGRES_USER=unicorn_user -e POSTGRES_PASSWORD=magical_password -d postgres:13-alpine
createdb:
	docker exec -it postgres13 createdb --username=unicorn_user --owner=unicorn_user warehouse

dropdb:
	docker exec -it postgres13 dropdb warehouse --username=unicorn_user

migrateup:
	migrate -path storage/migrations -database "postgresql://unicorn_user:magical_password@localhost/warehouse?sslmode=disable" -verbose up

migratedown:
	migrate -path storage/migrations -database "postgresql://unicorn_user:magical_password@localhost/warehouse?sslmode=disable" -verbose down

.PHONY: postgres createdb dropdb migrateup migratedown
