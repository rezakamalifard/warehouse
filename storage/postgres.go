package storage

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/lib/pq"
)

type postgresStore struct {
	db *sql.DB
}

const articlesStockConstraint = "articles_stock_check"

func openDB(user, password, dbname, sslmode string) (*sql.DB, error) {
	var connStr string
	if dbname == "" {
		connStr = fmt.Sprintf("user=%s password=%s sslmode=%s", user, password, sslmode)
	} else {
		connStr = fmt.Sprintf("user=%s password=%s dbname=%s sslmode=%s", user, password, dbname, sslmode)
	}
	// TODO set postgres pool related configs
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, fmt.Errorf("db open failed %w", err)
	}

	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("db connection error %w", err)
	}

	return db, nil

}

func newPostgresStore() (*postgresStore, error) {
	// read the db env
	var config Config
	if err := cleanenv.ReadEnv(&config); err != nil {
		return nil, fmt.Errorf("failed to load configs from env: %w", err)
	}
	db, err := openDB(config.PostgresDBUser, config.PostgresDBPassword, config.PostgresDBName, config.PostgresDBSSLMode)
	if err != nil {
		return nil, err
	}
	return &postgresStore{db: db}, nil

}

func (p postgresStore) StoreArticle(id uint64, name string, stock uint64) error {
	_, err := p.db.Exec(storeArticle, id, name, stock)
	return err
}

func (p postgresStore) StoreProduct(name string, productArticles map[uint64]uint64) error {
	var productID uint64
	tx, err := p.db.Begin()
	if err != nil {
		return err
	}
	if err := tx.QueryRow(storeProduct, name).Scan(&productID); err != nil {
		_ = tx.Rollback()
		return err
	}

	for articleID, amount := range productArticles {
		_, err := tx.Exec(storeProductArticles, productID, articleID, amount)
		if err != nil {
			_ = tx.Rollback()
			return err
		}
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func (p postgresStore) ListAllProducts() ([]byte, error) {
	var products []product
	rows, err := p.db.Query(listAllProducts)
	if err != nil {
		return nil, err
	}
	defer func() { _ = rows.Close() }()

	entry := product{}
	for rows.Next() {
		if err := rows.Scan(&entry.ID, &entry.Name, &entry.Availability); err != nil {
			return nil, err
		}
		products = append(products, entry)
	}

	return json.Marshal(products)
}

func (p postgresStore) SellProduct(name string) (existed, successful bool, err error) {
	tx, err := p.db.Begin()
	if err != nil {
		return false, false, err
	}
	var productID uint64
	err = tx.QueryRow(findProduct, name).Scan(&productID)
	if err != nil {
		_ = tx.Rollback()
		if err == sql.ErrNoRows {
			return false, false, nil
		}
		return false, false, err
	}
	_, err = tx.Exec(sellProduct, productID)
	if err != nil {
		_ = tx.Rollback()
		switch err := err.(type) {
		case *pq.Error:
			if err.Constraint == articlesStockConstraint {
				return true, false, nil
			}
			return true, false, err
		default:
			return true, false, err
		}
	}
	if err := tx.Commit(); err != nil {
		return true, false, err
	}
	return true, true, nil
}
