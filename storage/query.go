package storage

type product struct {
	ID           uint64 `json:"id"`
	Name         string `json:"name"`
	Availability uint64 `json:"availability"`
}

// TODO : prepare these queries to improve the performance
var (
	storeArticle         = `INSERT INTO articles (id, name, stock) VALUES ($1, $2, $3)`
	storeProduct         = `INSERT INTO products (name) VALUES ($1) RETURNING id`
	storeProductArticles = `INSERT INTO product_articles (product_id, article_id, amount) VALUES ($1, $2, $3)`
	listAllProducts      = `SELECT p.id, p.name, min(ar.stock / pa.amount) FROM product_articles pa, articles ar, products p WHERE pa.article_id = ar.id AND p.id = pa.product_id GROUP BY p.name, p.id`
	findProduct          = `SELECT id FROM products WHERE name = $1`
	sellProduct          = `UPDATE articles ar SET stock = stock - amount FROM product_articles pa WHERE ar.id = pa.article_id AND pa.product_id = $1`
)
