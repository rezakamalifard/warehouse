CREATE TABLE IF NOT EXISTS articles
(
    id    integer PRIMARY KEY NOT NULL,
    name  varchar             NOT NULL,
    stock integer             NOT NULL CHECK (stock >= 0)
);
CREATE TABLE IF NOT EXISTS products
(
    id    serial PRIMARY KEY,
    name  varchar NOT NULL,
    price numeric DEFAULT 0 CHECK (price >= 0)
);
CREATE TABLE IF NOT EXISTS product_articles
(
    product_id integer REFERENCES products (id),
    article_id integer REFERENCES articles (id),
    amount     integer NOT NULL
)
