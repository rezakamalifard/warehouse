package storage

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/ilyakaznacheev/cleanenv"
	"github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	"sync"
	"testing"
)

const (
	testDBName = "test_db"
)

func TestStoreArticleSuccessful(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer func() { _ = db.Close() }()

	// prepare stub for test
	mock.ExpectExec(`INSERT INTO articles`).WithArgs(1, "Table", 10).WillReturnResult(sqlmock.NewResult(1, 1))

	// create mock store
	store := &postgresStore{db: db}

	err = store.StoreArticle(1, "Table", 10)
	assert.NoError(t, err)

	// we make sure that all expectations were met
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestStoreProductSuccessful(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer func() { _ = db.Close() }()

	// expect transaction begin
	mock.ExpectBegin()

	// create mock store
	store := &postgresStore{db: db}

	productArticles := map[uint64]uint64{
		1: 5,
		2: 10,
	}

	mock.ExpectQuery("INSERT INTO products (.+) RETURNING id").
		WithArgs("product-name").
		WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(4))

	mock.ExpectExec("INSERT INTO product_articles").WithArgs(4, 1, 5).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("INSERT INTO product_articles").WithArgs(4, 2, 10).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	err = store.StoreProduct("product-name", productArticles)
	assert.NoError(t, err)

	// we make sure that all expectations were met
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestListAllProductsSuccessful(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer func() { _ = db.Close() }()

	rows := sqlmock.NewRows([]string{"id", "name", "amount"}).
		AddRow(1, "Table", 10).
		AddRow(5, "Chair", 20)
	mock.ExpectQuery("SELECT (.+) FROM product_articles pa, articles ar, products p WHERE pa.article_id = ar.id AND p.id = pa.product_id GROUP BY p.name, p.id").
		WithArgs().
		WillReturnRows(rows)

	// create mock store
	store := &postgresStore{db: db}
	response, err := store.ListAllProducts()
	assert.NoError(t, err)

	products := []product{
		{
			ID:           1,
			Name:         "Table",
			Availability: 10,
		},
		{
			ID:           5,
			Name:         "Chair",
			Availability: 20,
		},
	}

	expected, _ := json.Marshal(products)
	assert.Equal(t, expected, response)

	// we make sure that all expectations were met
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestSellProductSuccessful(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer func() { _ = db.Close() }()

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT (.+) FROM products").
		WithArgs("product-name").WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(4))

	mock.ExpectExec("UPDATE articles").WithArgs(4).WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectCommit()

	// create mock store
	store := &postgresStore{db: db}
	existed, successful, err := store.SellProduct("product-name")

	assert.NoError(t, err)
	assert.True(t, existed)
	assert.True(t, successful)

	// we make sure that all expectations were met
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestSellProductShouldFailForNotExistedProduct(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer func() { _ = db.Close() }()

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT (.+) FROM products").
		WithArgs("product-name").WillReturnError(sql.ErrNoRows)
	mock.ExpectRollback()

	// create mock store
	store := &postgresStore{db: db}
	existed, successful, err := store.SellProduct("product-name")

	assert.NoError(t, err)
	assert.False(t, existed)
	assert.False(t, successful)

	// we make sure that all expectations were met
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestSellProductShouldBeUnsuccessfulWithFailedConstraint(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer func() { _ = db.Close() }()

	retErr := &pq.Error{Constraint: articlesStockConstraint}

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT (.+) FROM products").
		WithArgs("product-name").WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(4))

	mock.ExpectExec("UPDATE articles").WithArgs(4).WillReturnError(retErr)

	// create mock store
	store := &postgresStore{db: db}
	existed, successful, err := store.SellProduct("product-name")

	assert.NoError(t, err)
	assert.True(t, existed)
	assert.False(t, successful)

	// we make sure that all expectations were met
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestSellProductShouldFailWithError(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer func() { _ = db.Close() }()

	retErr := errors.New("an-error")

	mock.ExpectBegin()
	mock.ExpectQuery("SELECT (.+) FROM products").
		WithArgs("product-name").WillReturnRows(sqlmock.NewRows([]string{"id"}).AddRow(4))

	mock.ExpectExec("UPDATE articles").WithArgs(4).WillReturnError(retErr)

	// create mock store
	store := &postgresStore{db: db}
	existed, successful, err := store.SellProduct("product-name")

	assert.Error(t, err)
	assert.True(t, existed)
	assert.False(t, successful)

	// we make sure that all expectations were met
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestSellProductShouldFailConcurrentUpdatesWithFailedConstraint(t *testing.T) {
	// skip this test in short mode it's related to database logic and need db config to run
	if testing.Short() {
		t.Skip("skip this test in short mode, this test needs database connection")
	}

	testStore, db := newTestStore(t)
	defer dropTestStore(t, testStore, db)

	goroutineCount := 10
	var failedCount int
	var wg sync.WaitGroup
	wg.Add(goroutineCount)
	for i := 0; i < goroutineCount; i++ {
		go func() {
			existed, successful, err := testStore.SellProduct("Dinning Table")
			if err != nil {
				t.Errorf("got an unexpected error %s", err)
			}
			if !existed {
				t.Errorf("product not found on the database")
			}
			if !successful {
				failedCount++
			}
			wg.Done()
		}()
	}
	wg.Wait()

	assert.Equal(t, goroutineCount-1, failedCount)
}

func newTestStore(t *testing.T) (*postgresStore, *sql.DB) {
	t.Helper()

	// read the db env
	var config Config
	err := cleanenv.ReadEnv(&config)
	assert.NoError(t, err)
	db, err := openDB(config.PostgresDBUser, config.PostgresDBPassword, "", config.PostgresDBSSLMode)
	assert.NoError(t, err)
	// create test database
	_, err = db.Exec("create database " + testDBName)
	assert.NoError(t, err)
	testDB, err := openDB(config.PostgresDBUser, config.PostgresDBPassword, testDBName, config.PostgresDBSSLMode)
	assert.NoError(t, err)
	// migrate the database
	driver, err := postgres.WithInstance(testDB, &postgres.Config{})
	assert.NoError(t, err)
	m, err := migrate.NewWithDatabaseInstance(
		fmt.Sprintf("file://%s", "./test-migrations"),
		"postgres",
		driver,
	)
	assert.NoError(t, err)
	// run the migrations
	err = m.Up()
	assert.NoError(t, err)

	return &postgresStore{db: testDB}, db
}

func dropTestStore(t *testing.T, store *postgresStore, db *sql.DB) {
	err := store.db.Close()
	assert.NoError(t, err)

	_, err = db.Exec("drop database " + testDBName + " WITH (FORCE)")
	assert.NoError(t, err)
}
