package storage

// Storage is used to Store Articles, Products and provides methods to list and sell them
type Storage interface {
	StoreArticle(id uint64, name string, stock uint64) error
	StoreProduct(name string, productArticles map[uint64]uint64) error
	ListAllProducts() (response []byte, err error)
	SellProduct(string) (existed, successful bool, err error)
}

// NewStorage create new Store and returns it
func NewStorage() (Storage, error) {
	return newPostgresStore()
}
