package storage

// Config is used to get configs from environment variables
type Config struct {
	// postgres configs
	PostgresDBUser     string `env:"INSERT_DB_USER"`
	PostgresDBPassword string `env:"INSERT_DB_PASSWORD"`
	PostgresDBName     string `env:"INSERT_DB_NAME"`
	PostgresDBSSLMode  string `env:"INSERT_DB_SSL_MODE" env-default:"disable"`
}
