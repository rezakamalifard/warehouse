CREATE TABLE IF NOT EXISTS articles
(
    id    integer PRIMARY KEY NOT NULL,
    name  varchar             NOT NULL,
    stock integer             NOT NULL CHECK (stock >= 0)
);
CREATE TABLE IF NOT EXISTS products
(
    id    serial PRIMARY KEY,
    name  varchar NOT NULL,
    price numeric DEFAULT 0 CHECK (price >= 0)
);
CREATE TABLE IF NOT EXISTS product_articles
(
    product_id integer REFERENCES products (id),
    article_id integer REFERENCES articles (id),
    amount     integer NOT NULL
);

INSERT INTO articles (id, name, stock)
VALUES (1, 'leg', 12);
INSERT INTO articles (id, name, stock)
VALUES (2, 'screw', 17);
INSERT INTO articles (id, name, stock)
VALUES (3, 'seat', 2);
INSERT INTO articles (id, name, stock)
VALUES (4, 'table top', 1);

INSERT INTO products (id, name, price)
VALUES (1, 'Dining Chair', 0);
INSERT INTO products (id, name, price)
VALUES (2, 'Dinning Table', 0);

INSERT INTO product_articles (product_id, article_id, amount)
VALUES (1, 2, 8);
INSERT INTO product_articles (product_id, article_id, amount)
VALUES (1, 3, 1);
INSERT INTO product_articles (product_id, article_id, amount)
VALUES (1, 1, 4);
INSERT INTO product_articles (product_id, article_id, amount)
VALUES (2, 1, 4);
INSERT INTO product_articles (product_id, article_id, amount)
VALUES (2, 2, 8);
INSERT INTO product_articles (product_id, article_id, amount)
VALUES (2, 4, 1);
