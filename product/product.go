package product

import (
	"encoding/json"
	"fmt"
	"go.uber.org/zap"
	"io/ioutil"
	"os"
	"strconv"
)

type store interface {
	StoreProduct(name string, productArticles map[uint64]uint64) error
}

// Product type is used to get store data and insert product records to it
type Product struct {
	Store store
}

// NewProduct creates a new Product instance assign it's variables and return it
func NewProduct(s store) *Product {
	return &Product{Store: s}
}

type productFormat struct {
	Products []products `json:"products"`
}
type article struct {
	ArtID    string `json:"art_id"`
	AmountOf string `json:"amount_of"`
}
type products struct {
	Name            string    `json:"name"`
	ContainArticles []article `json:"contain_articles"`
}

// Insert reads a json file, parse it using productFormat and insert its products on the store
func (p *Product) Insert(path string) error {
	var successful, failed uint64
	f, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("cannot open the products file %w", err)
	}

	// TODO: read the file using a buffer and tokens to support a big json file
	data, err := ioutil.ReadAll(f)
	if err != nil {
		return fmt.Errorf("cannot open the products file %w", err)
	}

	var input productFormat
	if err := json.Unmarshal(data, &input); err != nil {
		return fmt.Errorf("cannot unmarshal the products file %w", err)
	}

	for _, product := range input.Products {
		productArticles := make(map[uint64]uint64)
		for _, article := range product.ContainArticles {
			id, err1 := strconv.ParseUint(article.ArtID, 10, 64)
			amount, err2 := strconv.ParseUint(article.AmountOf, 10, 64)
			if err1 != nil || err2 != nil {
				failed++
				zap.L().Warn("converting values failed", zap.Errors("errors", []error{err1, err2}))
				continue
			}
			productArticles[id] = amount
			successful++
		}
		if err := p.Store.StoreProduct(product.Name, productArticles); err != nil {
			return fmt.Errorf("failed to insert product to store %w", err)
		}
	}
	zap.L().Info("product inserted", zap.Uint64("successful", successful), zap.Uint64("failed", failed))
	return nil
}
