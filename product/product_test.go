package product

import (
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
)

// TODO: add more tests to cover the error cases

type mockStore struct {
	storeProductFunc func(name string, productArticles map[uint64]uint64) error
}

func (ms mockStore) StoreProduct(name string, productArticles map[uint64]uint64) error {
	return ms.storeProductFunc(name, productArticles)
}

const productFileName = "products.json"

func createDirIfNotExist(t *testing.T, dir string) {
	t.Helper()
	_, err := os.Stat(dir)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(dir, os.ModePerm)
		assert.NoError(t, errDir)
	}
}

func generateMockFile(t *testing.T, record productFormat) (path string) {
	t.Helper()

	dir := os.TempDir()
	productDir := dir + "product-tests/"
	createDirIfNotExist(t, productDir)
	fileName := productDir + productFileName
	b, err := json.Marshal(record)
	_ = ioutil.WriteFile(fileName, b, 0644)

	assert.NoError(t, err)

	return fileName
}

func TestProductInsert(t *testing.T) {
	record := productFormat{Products: []products{
		{
			Name: "Dining Chair",
			ContainArticles: []article{
				{
					ArtID:    "1",
					AmountOf: "4",
				},
				{
					ArtID:    "2",
					AmountOf: "8",
				},
				{
					ArtID:    "3",
					AmountOf: "1",
				},
			},
		},
		{
			Name: "Dinning Table",
			ContainArticles: []article{
				{
					ArtID:    "1",
					AmountOf: "4",
				},
				{
					ArtID:    "2",
					AmountOf: "8",
				},
				{
					ArtID:    "4",
					AmountOf: "1",
				},
			},
		},
	}}
	path := generateMockFile(t, record)
	var names []string
	storeFunc := func(name string, productArticles map[uint64]uint64) error {
		names = append(names, name)
		return nil
	}

	store := mockStore{
		storeProductFunc: storeFunc,
	}

	product := NewProduct(store)
	err := product.Insert(path)

	assert.NoError(t, err)
	assert.Equal(t, []string{"Dining Chair", "Dinning Table"}, names)
}
