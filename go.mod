module gitlab.com/rezakamalifard/warehouse

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/gorilla/mux v1.8.0
	github.com/ilyakaznacheev/cleanenv v1.2.5
	github.com/lib/pq v1.9.0
	github.com/stretchr/testify v1.5.1
	go.uber.org/zap v1.16.0
)
