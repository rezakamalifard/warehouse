package api

import (
	"encoding/json"
	"go.uber.org/zap"
	"net/http"
	"time"
)

const (
	failedToGetProductDataErr = "failed to get products data"
	badRequestErr             = "bad request"
	requestNoNameKeyErr       = "no name key in the request post values"
	failedToSellProductErr    = "failed to sell the product"
	successfulProductSell     = "successful product sell"
	productNotFound           = "product not found"
	productNotAvailable       = "product is not available"
)

func productListHandler(s store) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		started := time.Now()
		zap.L().Info("got a list product request from", zap.String("remove_address", r.RemoteAddr))

		w.Header().Set("Content-Type", "application/json")

		products, err := s.ListAllProducts()
		if err != nil {
			zap.L().Error("failed to get the product list", zap.Errors("errors", []error{err}))
			writeError(w, http.StatusInternalServerError, failedToGetProductDataErr)
			return
		}

		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(products)
		duration := time.Since(started)

		zap.L().Info("list product request handled successfully", zap.String("duration", duration.String()))
	}
}

func sellProductHandler(s store) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		started := time.Now()
		zap.L().Info("got a sell product request from", zap.String("remove_address", r.RemoteAddr))
		w.Header().Set("Content-Type", "application/json")

		err := r.ParseForm()
		if err != nil {
			zap.L().Error("failed to parse form data", zap.Errors("errors", []error{err}))
			writeError(w, http.StatusBadRequest, badRequestErr)
			return
		}
		productName := r.FormValue("name")
		if productName == "" {
			zap.L().Info("got a bad request without name key")
			writeError(w, http.StatusBadRequest, requestNoNameKeyErr)
			return
		}
		existed, successful, err := s.SellProduct(productName)
		if err != nil {
			zap.L().Error("failed to update db to sell the product", zap.Errors("errors", []error{err}))
			writeError(w, http.StatusInternalServerError, failedToSellProductErr)
			return
		}
		if !existed {
			writeError(w, http.StatusNotFound, productNotFound)
			return
		}
		if !successful {
			writeError(w, http.StatusNotAcceptable, productNotAvailable)
			return
		}
		response := map[string]interface{}{
			"status":  http.StatusOK,
			"message": successfulProductSell,
		}
		w.WriteHeader(http.StatusOK)
		responseBody, _ := json.Marshal(response)
		_, _ = w.Write(responseBody)

		duration := time.Since(started)
		zap.L().Info("sell product request handled successfully", zap.String("duration", duration.String()))
	}
}

func writeError(w http.ResponseWriter, status int, message string) {
	response := map[string]interface{}{
		"status":  status,
		"message": message,
	}
	w.WriteHeader(status)
	responseBody, _ := json.Marshal(response)
	_, _ = w.Write(responseBody)
}
