package api

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/ilyakaznacheev/cleanenv"
	"go.uber.org/zap"
	"net/http"
)

type store interface {
	ListAllProducts() (response []byte, err error)
	SellProduct(name string) (existed, successful bool, err error)
}

// StartServer used to configure and server the API server
func StartServer(s store) error {
	var config Config
	if err := cleanenv.ReadEnv(&config); err != nil {
		return fmt.Errorf("failed to load server configs from env %w", err)
	}
	router := mux.NewRouter()

	router.HandleFunc("/products", productListHandler(s)).Methods("GET")
	router.HandleFunc("/products/sell", sellProductHandler(s)).Methods("POST")

	zap.L().Info("starting the server", zap.String("address", config.ServerAddress))

	return http.ListenAndServe(config.ServerAddress, router)
}
