package api

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type mockStore struct {
	listResult []byte
	listErr    error

	sellExisted    bool
	sellSuccessful bool
	sellErr        error
}

func (ms mockStore) ListAllProducts() (response []byte, err error) {
	return ms.listResult, ms.listErr
}

func (ms mockStore) SellProduct(name string) (existed, successful bool, err error) {
	return ms.sellExisted, ms.sellSuccessful, ms.sellErr
}

func TestProductListHandlerSuccess(t *testing.T) {
	store := mockStore{listResult: []byte{}, listErr: nil}
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/products", nil)
	handler := http.HandlerFunc(productListHandler(store))

	handler.ServeHTTP(recorder, req)

	response := make(map[string]interface{})
	_ = json.NewDecoder(recorder.Result().Body).Decode(&response)

	assert.Equal(t, http.StatusOK, recorder.Result().StatusCode)
}

func TestProductListHandlerError(t *testing.T) {
	listErr := errors.New("list-err")
	store := mockStore{listErr: listErr, listResult: nil}
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/products", nil)
	handler := http.HandlerFunc(productListHandler(store))

	handler.ServeHTTP(recorder, req)

	response := make(map[string]interface{})
	_ = json.NewDecoder(recorder.Result().Body).Decode(&response)

	assert.Equal(t, http.StatusInternalServerError, recorder.Result().StatusCode)
	assert.Equal(t, failedToGetProductDataErr, response["message"])
}

func TestSellProductHandlerSuccess(t *testing.T) {
	store := mockStore{sellExisted: true, sellSuccessful: true, sellErr: nil}
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/products/sell", strings.NewReader("name=SampleName"))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	handler := http.HandlerFunc(sellProductHandler(store))

	handler.ServeHTTP(recorder, req)

	assert.Equal(t, http.StatusOK, recorder.Result().StatusCode)

}

func TestSellProductHandlerNotExisted(t *testing.T) {
	store := mockStore{sellExisted: false, sellSuccessful: false, sellErr: nil}
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/products/sell", strings.NewReader("name=SampleName"))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	handler := http.HandlerFunc(sellProductHandler(store))

	handler.ServeHTTP(recorder, req)

	response := make(map[string]interface{})
	_ = json.NewDecoder(recorder.Result().Body).Decode(&response)

	assert.Equal(t, http.StatusNotFound, recorder.Result().StatusCode)
	assert.Equal(t, productNotFound, response["message"])

}

func TestSellProductHandlerError(t *testing.T) {
	sellError := errors.New("sell-err")
	store := mockStore{sellExisted: false, sellErr: sellError}
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/products/sell", strings.NewReader("name=SampleName"))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	handler := http.HandlerFunc(sellProductHandler(store))

	handler.ServeHTTP(recorder, req)

	response := make(map[string]interface{})
	_ = json.NewDecoder(recorder.Result().Body).Decode(&response)

	assert.Equal(t, http.StatusInternalServerError, recorder.Result().StatusCode)
	assert.Equal(t, failedToSellProductErr, response["message"])

}

func TestSellProductHandlerNotSuccessful(t *testing.T) {
	store := mockStore{sellExisted: true, sellSuccessful: false, sellErr: nil}
	recorder := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/products/sell", strings.NewReader("name=SampleName"))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	handler := http.HandlerFunc(sellProductHandler(store))

	handler.ServeHTTP(recorder, req)

	response := make(map[string]interface{})
	_ = json.NewDecoder(recorder.Result().Body).Decode(&response)

	assert.Equal(t, http.StatusNotAcceptable, recorder.Result().StatusCode)
	assert.Equal(t, productNotAvailable, response["message"])

}
